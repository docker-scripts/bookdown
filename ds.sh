cmd_make() {
    ds create
    ds config

    update_network_aliases
}

update_network_aliases() {
    local domain aliases=''
    for file in $(ls apache2/sites-enabled); do
        domain=${file%.conf}
        aliases+=" --alias $domain"
    done

    docker network disconnect --force $NETWORK $CONTAINER
    docker network connect $aliases $NETWORK $CONTAINER

    ds @revproxy restart
}
