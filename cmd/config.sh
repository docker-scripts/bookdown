cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    # create a sample book
    if [[ ! -d books ]]; then
        mkdir books
        git clone https://github.com/rstudio/bookdown-demo
        mv bookdown-demo books/
        ds render books/bookdown-demo
        ds site add bookdown-demo.example.org books/bookdown-demo
    fi
}
