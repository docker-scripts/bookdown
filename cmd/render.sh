cmd_render_help() {
    cat <<_EOF
    render <bookdir> [<format>]
        Render the given book on the given format.
        The format can be one of: html, pdf, epub
        If <format> is missing, all three of them will be rendered.

        If the book has a script named './render.sh', that one
        will be used instead.

_EOF
}

cmd_render() {
    [[ -z $1 ]] && fail "Usage:\n$(cmd_render_help)\n"
    ds inject render.sh "$@"
}
