cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    chmod +x .
    mkdir -p apache2/{sites-available,sites-enabled}
    orig_cmd_create \
        --mount type=bind,src=$(pwd)/apache2/sites-available,dst=/etc/apache2/sites-available \
        --mount type=bind,src=$(pwd)/apache2/sites-enabled,dst=/etc/apache2/sites-enabled
}
