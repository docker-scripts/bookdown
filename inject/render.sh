#!/bin/bash -xe

usage() {
    echo "
$0 <bookdir> [<format>]

Render the given book in the given format.
The format can be one of: html, pdf, epub
If <format> is missing, all three of them will be rendered.

If the book has a script named './render.sh', that one
will be used instead.

"
    exit 1
}

# go to the directory
bookdir=$1 ; shift
[[ -z $bookdir ]] && usage
cd $bookdir

if [[ -x render.sh ]]; then
    ./render.sh "$*"
else
    formats="$*"
    [[ -z $formats ]] && formats="html pdf epub"
    for format in $formats; do
        case $format in
            #html) Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::tufte_html_book')" ;;
            #html) Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::html_book')" ;;
            html) Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::gitbook')" ;;
            pdf)  Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::pdf_book')" ;;
            epub) Rscript -e "bookdown::render_book('index.Rmd', 'bookdown::epub_book')" ;;
            *)    echo "Error: Unknown format '$format'" ; usage ;;
        esac
    done
fi
