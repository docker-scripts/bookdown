# bookdown container

https://bookdown.org/yihui/bookdown/

## Installation

  - First install `ds` and `revproxy`:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull bookdown`

  - Create a directory for the container: `ds init bookdown @bookdown`

  - Fix the settings: `cd /var/ds/bookdown/ ; vim settings.sh`

  - Make the container: `ds make`

## Try the demo book

To look at the demo book, add `127.0.0.1 bookdown-demo.example.org` to
`/etc/hosts` then open in browser: https://bookdown-demo.example.org/

The directory of this book is: `books/bookdown-demo`. If you make some
changes to it, you can rebuild the site with: `ds render
books/bookdown-demo`.

## Add another book

We can create another book like this:
```
git clone https://github.com/rstudio/bookdown-demo
mv bookdown-demo books/book2
ds render books/book2
ds site add book2.example.org books/book2
```

Note: If the domain is not a real one, add this line in `/etc/hosts`:
`127.0.0.1 book2.example.org`

## Remove a book

We can remove a book like this:
- First delete the apache2 site: `ds site del bookdown-demo.example.org`
- Then remove the book: `rm -rf books/bookdown-demo`

## Serve multiple books from the same site/domain

If you want to serve multiple books from the same site/domain, like
https://books.example.org/book-1/, https://books.example.org/book-2/,
https://books.example.org/book-3/, etc, you can do it like this:
```
git clone https://github.com/rstudio/bookdown-demo
cp -a bookdown-demo books/book-1
cp -a bookdown-demo books/book-2
cp -a bookdown-demo books/book-3
rm -rf bookdown-demo

ds render books/book-1
ds render books/book-2
ds render books/book-3

mkdir books/books.example.org
cd books/books.example.org/
ln -s ../book-1/public book-1
ln -s ../book-2/public book-2
ln -s ../book-3/public book-3
cd -

ds site add books.example.org books/books.example.org --multibook
# don't forget the --multibook option at the end
```

## Other commands

```
ds stop
ds start
ds shell
ds help
```
