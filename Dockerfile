include(bookworm)

RUN <<EOF
  ### install some other tools that may be useful
  apt install --yes git wget curl unzip rsync

  ### install R
  DEBIAN_FRONTEND=noninteractive \
  apt install --yes r-base

  ### install TinyTex
  Rscript -e "install.packages('tinytex')"
  Rscript -e "tinytex::install_tinytex()"

  ### install bookdown
  Rscript -e "install.packages('bookdown')"

  ### install LaTeX package 'koma-script'
  Rscript -e "tinytex::tlmgr_install('koma-script')"

  ### install pandoc
  apt install --yes \
      pandoc pandoc-citeproc pandoc-citeproc-preamble

  ### install apache2
  apt install --yes apache2
  a2enmod ssl
EOF
